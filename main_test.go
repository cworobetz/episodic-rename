package main

import (
	"testing"
)

func TestGetEpisode(t *testing.T) {

	tables := []struct {
		in  int
		out string
	}{
		{9, "09"},
		{10, "10"},
		{100, "100"},
	}

	for _, table := range tables {
		episode := getEpisode(table.in)
		if episode != table.out {
			t.Errorf("getEpisode() was incorrect, input: %d, got: %s, want: %s", table.in, episode, table.out)
		}
	}
}

func TestGetSeason(t *testing.T) {

	tables := []struct {
		in  int
		out string
	}{
		{9, "09"},
		{10, "10"},
		{100, "100"},
	}

	for _, table := range tables {
		season := getSeason(table.in)
		if season != table.out {
			t.Errorf("getSeason() was incorrect, input: %d, got: %s, want: %s", table.in, season, table.out)
		}
	}
}

func TestGetFileName(t *testing.T) {

	tables := []struct {
		showTitle string
		season    int
		episode   int
		filePath  string
		out       string
	}{
		{"The ABC Show", 1, 2, "S01E02 The ABC Show.mp4", "The ABC Show - S01E02.mp4"},
		{"The 123 Show", 99, 33, "The 123 Show S99E33.jlk.mp4", "The 123 Show - S99E33.mp4"},
	}

	for _, table := range tables {
		season := getSeason(table.season)
		episode := getEpisode(table.episode)
		ext := getExt(table.filePath)
		fileName := getFileName(table.showTitle, season, episode, ext)
		if fileName != table.out {
			t.Errorf("getSeason() was incorrect, input: \"%+v\", got: \"%s\", want: \"%s\"", table, fileName, table.out)
		}
	}
}
