package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/Songmu/prompter"
)

func main() {

	// Intro, and usage flags
	fmt.Println("Episodic Rename")

	seasonFlag := flag.Int("season", -1, "The season you're using this on. Mandatory")
	showtitleFlag := flag.String("title", "", "The title of the show you're operating on. Mandatory")
	extFlag := flag.String("extension", "", "The file extension, e.g. \".mp4\" . Optional")

	flag.Parse()

	if *seasonFlag <= -1 { // Negative seasons don't exist
		log.Fatalln("Error: Specifying a season is mandatory.")
	}

	if *showtitleFlag == "" { // We don't try to automatically parse the title, for a dumb program it's too prone to error
		log.Fatalln("Error: Specifying the title is mandatory.")
	}

	// App logic below this point

	var ext string = getExt(*extFlag)               // File extension
	var season string = getSeason(*seasonFlag)      // Season of the show
	var showTitle string = getTitle(*showtitleFlag) // Title of the show
	var episode string                              // Episode of the season

	files, err := ioutil.ReadDir(".") // Read files from the user's current directory
	if err != nil {
		log.Fatal(err)
	}

	// Loop over all the files. We assume the files are listed in the correct order already, starting at E01
	for i, file := range files {
		episode = getEpisode(i + 1) // Episodes start at 1
		ext = getExt(file.Name())
		newFileName := getFileName(showTitle, season, episode, ext)
		fmt.Printf("%s => %s\n", file.Name(), newFileName)
		i++
	}
	if prompter.YN("Does this look correct?", false) {
		for i, file := range files {
			episode = getEpisode(i + 1)
			newFileName := getFileName(showTitle, season, episode, ext)
			err := os.Rename(file.Name(), newFileName)
			if err != nil {
				log.Fatal(err)
			}
		}
	}
}

// getEpisode returns a properly formatted episode string, following the convention S00E00
func getEpisode(episode int) string {
	if episode < 10 {
		return fmt.Sprintf("0%d", episode)
	}
	return fmt.Sprintf("%d", episode)
}

// getSeason returns a properly formatted season string, following the convention S00E00
func getSeason(season int) string {
	if season < 10 {
		return fmt.Sprintf("0%d", season)
	}
	return fmt.Sprintf("%d", season)
}

// getShowTitle returns the title of the show as a string
func getTitle(title string) string {
	// Clean up the title, if necessary
	return strings.Trim(title, " ")
}

// getExt returns the extension of the file as a string, without the period.
func getExt(fileName string) string {
	return strings.TrimLeft(filepath.Ext(fileName), ".")
}

// getFileName returns a properly formatted file name in the format "<showtitle> - S<season>E<episode>.<file extension>"
func getFileName(showTitle string, season string, episode string, ext string) string {
	return fmt.Sprintf("%s - S%sE%s.%s", showTitle, season, episode, ext)
}
