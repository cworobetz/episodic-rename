# Episodic-rename

Episodic rename is a simple program that may help you quickly rename episodes in a scanner-friendly format:

`<Show Title> - S<Season>E<Episode>.<Extension>`

For example, a renamed episode might be:

`The ABC Show - S03E10.mp4`

This program assumes that the directory already has the episodes in a sorted format.

## Usage

Change directory to the season the you wish to rename. Delete or move out any extra files, such as subtitles, pictures, folders, or special episodes.

```bash
episodic-rename -season 6 -title "The ABC Show"
```

It will prompt you to ensure the output is correct:

```bash
...
06.22 - Prefinale - The ABC Show.mp4 => The ABC Show - S06E22.mp4
06.23 - Finale - The ABC Show.mp4 => The ABC Show - S06E23.mp4
Does this look correct? (y/n) [n]:
```

## Platforms

### Linux

This has been designed to work with Linux. You can download the binary from the releases page.

### OSX

This has not been tested on OSX, but is being built with cross-platform functionality in mind. If you have tested it and it works, please let me know.

### Windows

This has not been tested on Windows, but is being built with cross-platform functionality in mind. If you have tested it and it works, please let me know.

## Building on Linux

**Using go**
This requires an installation of [go](https://golang.org) ≥ 1.13.
```
$ git clone https://gitlab.com/cworobetz/episodic-rename
$ cd episodic-rename
$ go mod download
$ go build -a -o ./episodic-rename
```

**Using docker**
This requires an installation of docker
```
$ git clone https://gitlab.com/cworobetz/episodic-rename
$ cd episodic-rename
$ docker build -t cworobetz/episodic-rename:latest .
$ id=$(docker create cworobetz/episodic-rename:latest)
$ docker cp $id:/go/bin/episodic-rename ./episodic-rename
```

### License
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
    SOFTWARE.