FROM golang:alpine AS build-env

WORKDIR $GOPATH/src/gitlab.com/cworobetz/episodic-rename

ENV CGO_ENABLED=0

COPY go.mod .

RUN go mod download

COPY . .

RUN go build -a -o /go/bin/episodic-rename

FROM scratch
COPY --from=build-env /go/bin/episodic-rename /go/bin/episodic-rename

ENTRYPOINT ["/go/bin/episodic-rename"]